import tweepy
import os
import sys

consumer_key = os.environ['ItsJustMyTwitterAppKey']
consumer_secret = os.environ['ItsJustMyTwitterAppSecret']
access_token = os.environ['ItsJustMyTwitterAppAccessToken']
access_token_secret = os.environ['ItsJustMyTwitterAppAccessTokenSecret']

class Twitter():

    def __init__(self):
        auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
        auth.set_access_token(access_token, access_token_secret)
        self.api = tweepy.API(auth)

    def post(self, tweetFile=None):
        if tweetFile is not None:
            message = self.__split_message(open(os.path.join(os.getcwd(), tweetFile)).read().replace('\n', ' '))
        else:
            message = self.__split_message(input("Type your message to post on Twitter: "))
        message = [" ".join(part) for part in message]
        for tweet in message:
            self.api.update_status(tweet)


    def __split_message(self, message):
        message = message.split()
        length = 0
        index = 0
        messageToPost = [[]]
        for word in message:
            length += len(word) + 1
            if length <= 132:
                messageToPost[index].append(word)
            else:
                messageToPost[index].append("...({})".format(index+1))
                length = 0
                index += 1
                messageToPost.append([])
        messageToPost[index].append("({})".format(index + 1))
        return messageToPost

if __name__ == '__main__':
    twitter = Twitter()
    if sys.argv[1]:
        twitter.post(sys.argv[1])
    else:
        twitter.post()